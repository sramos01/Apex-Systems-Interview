package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.By.ByName;
import org.openqa.selenium.By.ByTagName;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;

public class loginPage {
	//Init WebDriver
	WebDriver driver;
	//Locators	
	By loginBtn = new By.ByXPath("//span[@class='a-header__topLink']");
	By accountBtn = new By.ByXPath("//a[contains(text(),'Crear')]");
	By email = new By.ByName("email");
	By password = new By.ByName("password");
	By createBtn = new By.ByName("action");
	By firstName = new By.ById("input-user__name");
	By lastName = new By.ById("input-user__apaterno");
	By yearSelect = new By.ById("yearSelectorLabel");
	By pIBtn = new By.ByTagName("button");
	By username = new By.ById("username");
	By error = new By.ById("error-element-email");
	
	public loginPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	public void clickLogin() {
		driver.findElement(loginBtn).click();	
		}
	public void clikcCreateAccount() {
		waitCreateAccount(accountBtn);
		driver.findElement(accountBtn).click();
	}
	public void waitCreateAccount(By locator) {
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}
	public void email(String userEmail) {
		driver.findElement(email).sendKeys(userEmail);
	}
	public void password(String userPwd) {
		driver.findElement(password).sendKeys(userPwd);
	}
	public void clickCreateBtn() {
		driver.findElement(createBtn).click();
	}
	public void name() {
		driver.findElement(firstName).sendKeys("Maria");
	}
	public void lastName() {
		driver.findElement(lastName).sendKeys("Ramirez");
	}
	public void year() {
		Select year = new Select(driver.findElement(yearSelect));
		year.selectByValue("2000");
	}
	public void submitPI() {
		driver.findElement(pIBtn).click();;
	}
	public void homePage(String homeUrl) {
		driver.get(homeUrl);
	}
	public String errorMsg() {
		return driver.findElement(error).getText();
	}
	//functions that are used twice, for creating account and for assertions
	public void createDoubleUsed(String userEmail, String userPwd, String homeUrl) {
		this.clickLogin();
		this.clikcCreateAccount();
		this.email(userEmail);
		this.password(userPwd);
		this.clickCreateBtn();
	}
	public String createFunction(String userEmail, String userPwd, String homeUrl) {
		this.createDoubleUsed(userEmail, userPwd, homeUrl);
		this.name();
		this.lastName();
		this.year();
		this.submitPI();
		this.homePage(homeUrl);
		this.createDoubleUsed(userEmail, userPwd, homeUrl);
		return this.errorMsg();
	}
	
	
}