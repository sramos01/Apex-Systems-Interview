package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.By.ByTagName;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class searchBar {
	//Initializing variables
	WebDriver driver;
	String text;
	
	//Locators
	By searchBarField = new By.ById("mainSearchbar");
	By lookUpButton = new By.ByClassName("input-group-text");
	By waitLocator = new By.ByXPath("//h1[@class=\"a-headline__typeahed searcherTitle-result\"]");
	By brandLocator	= new By.ByXPath("//*[@class=\"a-title-section-leftMenu\"]");
	By errorLocator = new By.ByXPath("//h1[@class='a-errorPage-title']");
	By noResultsLocator = new By.ByXPath("//h1/strong[1]");
	By assertionText = new By.ByXPath("//h1");
	
	public searchBar(WebDriver driver) {
		this.driver = driver;
	}
	
	public void searchInput(String product) {
		driver.findElement(searchBarField).sendKeys(product);
	}
	
	public void enter() {
		driver.findElement(searchBarField).sendKeys(Keys.ENTER);
	}
	
	public void searchFunction(String product) {
		this.searchInput(product);
		this.enter();
	}
	//Explicit wait
	public void pageWait(By locator) {
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}
	public String getText(int id) {
		//Positive outcome test cases
		if(id<=2 || id>=7) {
			pageWait(waitLocator);
			text = driver.findElement(assertionText).getText();
		}
		// Positive outcome - brand
		else if (id==6) {
			pageWait(brandLocator);
			text = driver.findElement(brandLocator).getText();
		}
		//Negative outcome
		else if (id==3) {
			pageWait(errorLocator);
			text= driver.findElement(assertionText).getText();
		}
		//Negative outcome - no results found
		else {
			pageWait(noResultsLocator);
			text = driver.findElement(noResultsLocator).getText();
					}
		return text;
	}


}
