package Pages;

import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.jupiter.api.Assertions.*;


public class loginPageTest {
	//Init objects and variables
	WebDriver driver;
	loginPage objLogin;
	String homeUrl = "https://www.liverpool.com.mx/tienda/home";
	
	public WebDriver setup() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\DANY_\\Downloads\\chromedriver_win321\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(homeUrl);
		return driver;
	}
	@Test
	public void test_loginPage() {
		driver = setup();
		String errorMsg="El correo electrónico ya ha sido registrado";
		String email = "daniela.ramos23@outlook.com"; //Different email everytime
		String pwd ="Validpassword1!";
		objLogin = new loginPage(driver);
		Assertions.assertEquals(objLogin.createFunction(email,pwd,homeUrl), errorMsg);
		driver.quit();
	}

}
