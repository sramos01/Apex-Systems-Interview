package Pages;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class buyTV {
	//Initializing variables
	WebDriver driver;
	String text;
	//Locators
	By brandText = new By.ById("search-filter-brands");
	By brandCheckBox = new By.ById("brand-SAMSUNG");
	By sizeCheckBox = new By.ById("variants.normalizedSize-32 pulgadas");
	By minPriceText = new By.ById("min-price-filter");
	By maxPriceText = new By.ById("max-price-filter");
	By priceButton = new By.ByXPath("//*[@class='a-price__filterButton']");
	By title= new By.ByXPath("//h5[@class='card-title a-card-description']");
	By price = new By.ByXPath("//p[@class='a-card-discount']");
	
	
	public buyTV(WebDriver driver) {
		this.driver = driver;
	}
	public void scroll(int amount) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,"+amount+")", "");		
	}
	public void minPriceFilter(int min) {
		driver.findElement(minPriceText).sendKeys(String.valueOf(min));
	}
	public void maxPriceFilter(int max) {
		driver.findElement(maxPriceText).sendKeys(String.valueOf(max));
	}
	public void clickPriceFilter() {
		driver.findElement(priceButton).click();
	}
	public void brandFilterText(String brand) {
		driver.findElement(brandText).sendKeys(brand);
	}
	public void brandFilterCheckbox() {
		driver.findElement(brandCheckBox).click();;
	}
	public void sizeFilterCheckbox() {
		driver.findElement(sizeCheckBox).click();
	}
	public String getproductTitle() {
		return driver.findElements(title).get(0).getText();
	}
	public int formatedPrice(int max) {
		return Integer.valueOf(getproductPrice().subSequence(1, String.valueOf(max).length()+2).toString().replace(",", ""));
	}
	
	public String getproductPrice() {
		return driver.findElements(price).get(0).getText();
	}
	
	
	public void buyTVFunction(String brand, int min, int max)  {
		this.scroll(2000);
		this.minPriceFilter(min);
		this.maxPriceFilter(max);
		this.clickPriceFilter();
		this.scroll(1000);
		this.brandFilterText(brand);
		this.brandFilterCheckbox();
		this.scroll(300);
		this.sizeFilterCheckbox();
		this.formatedPrice(max);
	}
	

}
