package Pages;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class buyTVTest {
	//init objects
	WebDriver driver;
	searchBar objSearch;
	buyTV objBuy;
	
	//Locator
	By waitLocator = new By.ByXPath("//h1[@class=\"a-headline__typeahed searcherTitle-result\"]");
	
	public WebDriver setup() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\DANY_\\Downloads\\chromedriver_win321\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.liverpool.com.mx/tienda/home");
		objSearch = new searchBar(driver);
		objSearch.searchFunction("Smart TV");
		objSearch.pageWait(waitLocator);
		return driver;
	}
	@Test
	public void test_buySamsungTV() {
		String brand = "Samsung";
		int maxPrice = 6000;
		driver = setup();
		objBuy = new buyTV(driver);
		objBuy.buyTVFunction(brand, 0, 6000);
		Assertions.assertTrue(objBuy.getproductTitle().contains(brand));
		Assertions.assertTrue(objBuy.getproductTitle().contains("32"));
		Assertions.assertTrue(objBuy.formatedPrice(maxPrice)<maxPrice);
		driver.quit();
	}

}
