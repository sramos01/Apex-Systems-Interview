package Pages;

import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class searchBarTest {
	WebDriver driver;
	searchBar objSearch;
	//Initialize Driver
	
	public WebDriver setup() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\DANY_\\Downloads\\chromedriver_win321\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.liverpool.com.mx/tienda/home");
		return driver;
	}
	
	//Test search bar functionality
	@ParameterizedTest
	@CsvFileSource (resources = "/searchData.csv")
	public void test_searchBar(int id, String product) {
		driver = setup();
		objSearch = new searchBar(driver);
		objSearch.searchFunction(product);
		String assertionText = objSearch.getText(id);
		if (id<3 || id>5) {
			//Positive outcome test cases
			Assertions.assertEquals(product.toUpperCase(), assertionText.toUpperCase().subSequence(0, product.length()));
		}
		if (id==3) {
			//Negative outcome test case
			String errorText = "Lo sentimos";
			Assertions.assertEquals(errorText,assertionText.subSequence(0, errorText.length()));
		}
		if (id==4 || id ==5) {
			// No results found
			Assertions.assertEquals(product.toUpperCase(), assertionText.toUpperCase().subSequence(1, product.length()+1));
		}
		driver.close();
		
	}
	

}
